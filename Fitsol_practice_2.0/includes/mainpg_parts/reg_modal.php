
<div class="modal fade col-sm-12" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Registry Form</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div class="input-group">
    <label>Fullname</label>
      <div id="fnameCont" class="form_element input-group-prepend">
          <input  type="text" id="fullname" class="form-control" placeholder="Fullname" aria-label="fullname" aria-describedby="addon-wrapping">
      </div>
      <label>Username</label>
      <div class="form_element input-group-prepend">
          <input type="text" id="username" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="addon-wrapping">
      </div>
      <label>Password</label>
      <div class="form_element input-group-prepend">  
          <input type="text" id="password" class="form-control" placeholder="Password" aria-label="password" aria-describedby="addon-wrapping">
      </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      <button type="button" id="regBtn" class="btn btn-success">Register</button>
    </div>
  </div>
</div>
</div>

