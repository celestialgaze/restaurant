<?php

use Phinx\Migration\AbstractMigration;

class ThirdMigration extends AbstractMigration
{
    
    public function change()
    {
        $table = $this->table('second_table');
        $table->addColumn('first_name','text')
                ->addColumn('second_name','text')
                ->addColumn('date_of_birth','date')
                ->addColumn('gender', 'text')
                ->create();
    }
}
