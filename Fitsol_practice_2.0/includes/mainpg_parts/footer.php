<div id="footer" class="footer col-sm-12 bg bg-dark d-flex flex-wrap align-items-center">
  <div class="col-lg-6 col-sm-12 blackbg d-flex flex-column">
    <div class="d-flex flex-column col-sm-12 p-5 text-center">
      <p class="p-3 bg bg-warning mt-0 mb-2 mr-auto ml-auto" style="width:100%">
        If you have any questions, please write to us!
      </p>
      <input id="usr_id" type="text" hidden>
      <input id="usr_email" class="mt-3 mb-2" type="email" placeholder="Your e-mail address"> 
      <textarea class="mt-3 mb-3" name="message" id="usr_msg" cols="5" rows="10" placeholder="Your message goes here"></textarea>
      <button id="btn_msg" class="mt-3 mb-4 btn btn-md btn-warning">Send</button>
    </div>
  </div>
  <div class="col-lg-6 col-sm-12 d-flex">
  <div class="col-sm-12 d-flex flex-column contact_container">
    <h2 class="bg bg-danger text-center mt-5 mb-5 p-3">Contacts</h2>
    <div class="d-flex flex-column justify-content-center bg bg-light pt-5 pb-5">
    <p class="mt-3 mb-3  text-center"><span><i class="fa fa-phone-square" aria-hidden="true"></i></span><span>Lorem ipsum dolor sit amet.</span><span>Lorem ipsum dolor sit amet.</span> </p>
    <p class="mt-3 mb-3 text-center"><span><i class="fa fa-skype" aria-hidden="true"></i></span><span>Lorem ipsum dolor sit amet.</span><span>Lorem ipsum dolor sit amet.</span></p>
    <p class="mt-3 mb-3 text-center"><span><i class="fa fa-mobile" aria-hidden="true"></i></span><span>Lorem ipsum dolor sit amet.</span><span>Lorem ipsum dolor sit amet.</span></p>
  </div>
</div>
</div>
    </div>