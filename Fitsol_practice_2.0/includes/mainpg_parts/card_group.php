<div class="card-group">
  <div class="card">
    <img src="assets/images/bg_menu.jpg" class="card-img-top" alt="menu">
    <div class="card-body">
      <h5 class="card-title">Menu Range</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
    </div>
    <div class="card-footer">
    <span class="btn btn-dark btn-md">Show Me >></span>
    </div>
  </div>
  <div class="card">
    <img src="assets/images/event.jpg" class="card-img-top" alt="event">
    <div class="card-body">
      <h5 class="card-title">Upcoming Events</h5>
      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
    </div>
    <div class="card-footer">
      <span class="btn btn-dark btn-md">Show Me >></span>
    </div>
  </div>
  <div class="card">
    <img src="assets/images/team.jpg" class="card-img-top" alt="team">
    <div class="card-body">
      <h5 class="card-title">Our Team</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
    </div>
    <div class="card-footer">
    <span class="btn btn-dark btn-md">Show Me >></span>
    </div>
  </div>
</div>