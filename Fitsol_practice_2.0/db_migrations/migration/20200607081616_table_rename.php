<?php

use Phinx\Migration\AbstractMigration;

class TableRename extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('restaurantGuests');
        $table
            ->rename('restaurant_guests')
            ->update();
    }
}
