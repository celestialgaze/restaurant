<?php

use Phinx\Migration\AbstractMigration;

class FifthMigration extends AbstractMigration
{
    public function up()
    {

    }
    
    public function down()
    {
        $table = $this->table('second_table');
        $table
            ->rename('users')
            ->update();
    }
}
