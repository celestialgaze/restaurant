<?php

use Phinx\Migration\AbstractMigration;

class FourthMigration extends AbstractMigration
{
    public function up() 
    {
        $rows = [
            [
            'first_name'=> 'Alexa',
            'second_name'=> 'Makacs',
            'date_of_birth'=> '1991-12-05',
            'gender'=> 'female'
            ],
            [
                'first_name'=> 'Eszter',
                'second_name'=> 'Makacs',
                'date_of_birth'=> '1992-12-05',
                'gender'=> 'female'
            ],
            [
                'first_name'=> 'Daniel',
                'second_name'=> 'Kakacs',
                'date_of_birth'=> '1993-12-05',
                'gender'=> 'male'
            ],
            [
                'first_name'=> 'Ödön',
                'second_name'=> 'Takacs',
                'date_of_birth'=> '1994-12-05',
                'gender'=> 'male'
            ],
            [
                'first_name'=> 'Diana',
                'second_name'=> 'Rakacs',
                'date_of_birth'=> '1995-12-05',
                'gender'=> 'female'
                ]
        ];
        $table = $this->table('second_table')->insert($rows)->save();
    }
}
