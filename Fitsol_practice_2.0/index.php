<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
     <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>
    <div class="container d-flex col-sm-10 ">
    <div class="wrapper flex-column col-sm-12 mr-auto ml-auto">
<?php
  include "includes/mainpg_parts/uppernav.php";
  //Nav
  include "includes/mainpg_parts/log_modal.php";
  //Modal
  include "includes/mainpg_parts/reg_modal.php";
  //Carousel
  include "includes/mainpg_parts/carousel.php";
?>
<hr>
<div class="col-sm-12">
  <div class="col-sm-12 p-0">
  <h2 class="text-center bg bg-dark text-light p-3">History of the Restaurant</h2>
  <hr>
  <div class="col-sm-12 pl-0 pr-0 d-flex flex-wrap">
    <div class="col-sm-12 col-lg-6 pl-0 mb-2">
    <img id="imgOfPlace" src="assets/images/restaurant2.jpg" class="responsive" width="100%" height="100%" alt="The Restaurant">
    </div>
    <div id="theStory" class="col-sm-12 col-lg-6 bg bg-light d-flex align-items-center">
    <p class="text-center jumbotron">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem sunt ad beatae voluptatem quasi perferendis est recusandae expedita quibusdam blanditiis suscipit obcaecati quisquam minima consequatur dolor ipsa, reiciendis velit molestiae quaerat tempora eius in. Officia illum asperiores minima. Et repellat cupiditate voluptatem ipsa non quia placeat at eos doloremque? Odio, consequuntur? Deserunt non culpa accusantium ipsum quis, modi, rerum tempore ex hic consequatur numquam, atque voluptatum eius sunt officiis ab? Quia, nesciunt quae! Molestiae, exercitationem pariatur rem voluptatibus vitae corrupti soluta voluptas quam quidem porro, 
    sapiente delectus doloribus magnam ex aut saepe. Velit accusantium explicabo non dolores sunt dolorum reprehenderit. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia, non earum! Fuga, laboriosam adipisci doloribus ullam eveniet suscipit architecto commodi veritatis fugit illo maxime dolorem. Quia voluptatum consectetur corrupti temporibus ab ea ipsum, tempora aperiam nobis labore deleniti dolor consequuntur voluptas cum eaque? Minima, tempora sequi saepe perspiciatis natus tempore cupiditate libero labore reprehenderit doloribus iste quam aut. Sunt beatae nostrum unde totam magni illum, blanditiis facilis quidem laboriosam quis explicabo eligendi. Nostrum doloremque minima omnis fugiat obcaecati cupiditate aliquam perferendis illum adipisci
     optio numquam molestiae distinctio ut nemo voluptates, id dolor at recusandae odio incidunt praesentium.</p>
    </div>
  </div>
  </div>
</div>
<?php
//Card Group
include "includes/mainpg_parts/card_group.php";
//Footer
include "includes/mainpg_parts/footer.php";
?>
  </div>
    </div>
<?php 
require "includes/functions/functions.php";
?>
</body>
</html>