<?php

use Phinx\Migration\AbstractMigration;

class TableCreator extends AbstractMigration
{
    
    public function change()
    {
        $table = $this->table("restaurantGuests");
        $table->addColumn('fullname','text')
            ->addColumn('username','string')
            ->addColumn('password','string')
            ->create();
        if ($this->isMigratingUp()) {
            $table->insert([['fullname' => 'Sipos Péter', 'username' => 'user', 'password' => 'user123' ]])
                    ->save();
        }
    }
}
