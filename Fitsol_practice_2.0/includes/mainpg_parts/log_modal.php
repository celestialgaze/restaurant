<div class="modal fade col-sm-12" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Login Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="input-group">
        <label>Username</label>
        <div class="form_element input-group-prepend">
            <input type="text" id="username2" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="addon-wrapping">
        </div>
        <label>Password</label>
        <div class="form_element input-group-prepend">  
            <input type="text" id="password2" class="form-control" placeholder="Password" aria-label="password" aria-describedby="addon-wrapping">
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" id="logBtn" class="btn btn-info">Log Me In</button>
      </div>
    </div>
  </div>
</div>