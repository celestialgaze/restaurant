<div id="carouselExampleSlidesOnly" class="carousel slide mt-5 mb-3" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <img src="assets/images/salad.jpg" id="bgSalad" class="d-block w-100" alt="salad">
    </div>
    <div class="carousel-item">
    <img src="assets/images/burger.jpg" id="bgBurger" class="d-block w-100" alt="burger">
    </div>
    <div class="carousel-item">
    <img src="assets/images/pizza.jpg" id="bgPizza" class="d-block w-100" alt="pizza">
    </div>
  </div>
</div>