<?php

use Phinx\Migration\AbstractMigration;

class AddExtraCloumns extends AbstractMigration
{
    
    public function up()
    {
        $table = $this->table('restaurant_guests');
        $table  ->addColumn('email','string',['limit'=>30, 'null'=>true])
                ->addColumn('message','string',['limit'=>255, 'null'=>true])
                ->save();

    }
    public function down()
    {

    }
}
