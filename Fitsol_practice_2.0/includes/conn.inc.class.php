<?php
define('DB_USERNAME', 'tanulj');
define('DB_PASSWORD', 'tanulj');
define('DSN', 'pgsql:host=localhost; dbname=tanulj;');
class Dbh {
    function connect(){
        try {
        $dbh = new PDO(DSN, DB_USERNAME, DB_PASSWORD);
        $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $dbh;
        } catch (PDOException $e) {
            print "Error!" .$e->getMessage(). "<br>";
        }
    }
}
?>
