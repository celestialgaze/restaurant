<?php

use Phinx\Migration\AbstractMigration;

class SecondMigration extends AbstractMigration
{
    
    public function up()
    {
        $dateNow = date('H:i');
        // inserting only one row
        $singleRow = [
            'user_id'    => 1,
            'created'  => '2020-01-19 03:14:07'
        ];

        $table = $this->table('user_logins');
        $table->insert($singleRow);
        $table->saveData();

        // inserting multiple rows
        $rows = [
            [
              'user_id'    => 2,
              'created'  => '2020-01-19 03:14:07'
            ],
            [
              'user_id'    => 3,
              'created'  => '2020-01-19 03:14:07'
            ]
        ];

        $this->table('user_logins')->insert($rows)->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute('DELETE FROM user_logins');
    }
    
}

